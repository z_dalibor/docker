# MySQL

## Building Image

```
docker build -t <repo>:<tag> .
```

## Create Contatiner

```
docker volume create <volume-name>
docker create --name mysql --network <network-name> -v <volume-name>:/var/lib/mysql -p 3306:3306 <image>
```
