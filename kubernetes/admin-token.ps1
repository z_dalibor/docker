# Get token for acces kubernetes dashboard for some user

$user = "admin-user"

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret |
    Select-String -Pattern "$user" |
    ForEach-Object { ($_ -split "\s+")[0] })
